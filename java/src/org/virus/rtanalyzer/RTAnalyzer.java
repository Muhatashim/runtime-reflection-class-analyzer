package org.virus.rtanalyzer;

import javafx.application.Application;
import org.virus.rtanalyzer.gui.MainUI;
import org.virus.rtanalyzer.utils.reflection.JarLoader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/29/13
 * Time: 10:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class RTAnalyzer {
    private static final ExecutorService pool = Executors.newFixedThreadPool(4);

    public static void main(String[] args) throws MalformedURLException, InterruptedException {
        submit(new Runnable() {
            @Override
            public void run() {
                Application.launch(MainUI.class);
            }
        });

        while (MainUI.instance == null)
            Thread.sleep(100);

        URL url = new File("C:\\Users\\VOLT\\Desktop\\arravclient.jar").toURI().toURL();
        try {
            MainUI.instance.setup(new JarLoader(url));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Future<?> submit(Runnable runnable) {
        return pool.submit(runnable);
    }

    public static <T> Future<T> submit(Callable<T> task) {
        return pool.submit(task);
    }
}
