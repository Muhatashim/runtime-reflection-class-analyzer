package org.virus.rtanalyzer.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.virus.rtanalyzer.utils.reflection.ReflectionUtils;

import javax.swing.*;
import java.lang.reflect.Field;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/29/13
 * Time: 6:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class GetField {

    private final Field field;


    private final GridPane grid;

    private final Label     fieldLabel;
    private final Label     fieldValue;
    private final TextField instanceForTF;
    private final Button    getButton;
    private final Stage     stage;

    public GetField(Field field) {
        this.field = field;
        grid = new GridPane();
        stage = new Stage();
        Scene scene = new Scene(grid);

        fieldLabel = new Label(field.getName());
        fieldValue = new Label();
        instanceForTF = new TextField();
        getButton = new Button("Get");

        addListeners();
        initUI();

        stage.setScene(scene);
        stage.show();
    }

    private void addListeners() {
        getButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Object instanceFor;
                try {
                    instanceFor = ReflectionUtils.getValue(instanceForTF.getText());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Couldn't find class!");
                    return;
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, "No such field!");
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

                try {
                    Object value = ReflectionUtils.getValue(field, instanceFor);
                    fieldValue.setText(ReflectionUtils.toString(value));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initUI() {
        VBox fieldProperties = new VBox(5);
        fieldProperties.setPadding(new Insets(10, 10, 10, 10));
        fieldProperties.setAlignment(Pos.CENTER);
        fieldProperties.getChildren().addAll(fieldLabel, fieldValue, instanceForTF, getButton);

        grid.add(fieldProperties, 0, 0);
    }

}
