package org.virus.rtanalyzer.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;


/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/2/13
 * Time: 10:17 AM
 */
public class InstanceFinderPane extends GridPane {
    private static final int DEPTH = 10;

    private ListView<Map.Entry<Field, Object>>       fqfnsView;
    private ObservableList<Map.Entry<Field, Object>> fieldsFound;
    private Button                                   findButton;

    private Map<Field, Object> fieldsMap;
    private Class<?>           forClass;

    public InstanceFinderPane() {
        findButton = new Button("Find");
        fieldsFound = FXCollections.observableArrayList();
        fqfnsView = new ListView<>(fieldsFound);

        fieldsMap = new HashMap<>();

        findButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                depthSearch();
                fieldsFound.clear();

                Set<Map.Entry<Field, Object>> entries = fieldsMap.entrySet();
                for (Map.Entry<Field, Object> entry : entries)
                    fieldsFound.add(entry);
            }
        });

        initGUI();
    }

    public void setForClass(Class<?> forClass) {
        this.forClass = forClass;
        fieldsMap.clear();
        fieldsFound.clear();
    }

    private void initGUI() {
        VBox topBox = new VBox();
        topBox.getChildren().addAll(findButton, new Label("Instances referenced:"));
        topBox.setPadding(new Insets(10, 10, 10, 10));
        topBox.setAlignment(Pos.CENTER);
        add(topBox, 0, 0);

        add(fqfnsView, 0, 1);
    }

    public Object getInstanceSelected() {
        System.out.println(fqfnsView.getSelectionModel().getSelectedItem().getValue());
        return fqfnsView.getSelectionModel().getSelectedItem().getValue();
    }

    private void depthSearch() {
        for (Class<?> clazz : MainUI.instance.getLoader().getClasses())
            depthSearch(new ArrayList<Integer>(), clazz, null, false, DEPTH);
    }

    private void depthSearch(List<Integer> refsChecked,
                             Class<?> currentParent, Object parentObject, boolean arrayElement, int depth) {

        Package aPackage = currentParent.getPackage();
        Class<?> componentType = currentParent.getComponentType();
        if (depth == 0
                || currentParent.isPrimitive()
                || componentType != null && componentType.isPrimitive()
                || aPackage != null && aPackage.getSpecificationVendor().equals("Oracle Corporation"))
            return;

        if (componentType != null && parentObject != null) {
            int len = Array.getLength(parentObject);
            if (len == 0)
                return;

            depthSearch(refsChecked, componentType,
                    Array.get(parentObject, 0), true, depth);
        }

        for (Field field : currentParent.getDeclaredFields()) {
            if (!Modifier.isStatic(field.getModifiers()) && parentObject == null)
                continue;

            try {
                field.setAccessible(true);
                Object value = field.get(parentObject);

                int ref = System.identityHashCode(value);
                if (ref != 0 && refsChecked.contains(ref))
                    continue;
                refsChecked.add(ref);

                if (forClass.isAssignableFrom(field.getType()))
                    fieldsMap.put(field, value);

                depthSearch(refsChecked, field.getType(), value, false, depth - 1);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
