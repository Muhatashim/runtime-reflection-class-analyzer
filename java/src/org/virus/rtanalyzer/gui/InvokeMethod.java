package org.virus.rtanalyzer.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.virus.rtanalyzer.utils.reflection.ReflectionUtils;

import javax.swing.*;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 8:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class InvokeMethod {
    private final GridPane grid;

    private final Label     methodNameLabel;
    private final Label     returnValueLabel;
    private final TextField objectFor;
    private final Method    method;
    private final Button    invokeButton;

    public InvokeMethod(Method method) {
        this.method = method;

        grid = new GridPane();

        methodNameLabel = new Label(method.toString());
        returnValueLabel = new Label();
        objectFor = new TextField();
        invokeButton = new Button("Invoke");

        if (Modifier.isStatic(method.getModifiers())) {
            objectFor.setText("null");
            objectFor.setEditable(false);
        }

        initUI();
        addListeners();

        Stage stage = new Stage();
        stage.setScene(new Scene(grid));
        stage.show();
    }

    private void addListeners() {
        invokeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Object instanceFor;
                if (!Modifier.isStatic(method.getModifiers())) {
                    try {
                        instanceFor = ReflectionUtils.getValue(objectFor.getText());
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Couldn't find class!");
                        return;
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(null, "No such field!");
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                } else
                    instanceFor = null;

                try {
                    Object invoke = ReflectionUtils.invoke(method, instanceFor);
                    if (invoke != null)
                        returnValueLabel.setText(invoke.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    returnValueLabel.setText("Error");
                }
            }
        });
    }

    private void initUI() {
        VBox methodNameBox = new VBox();
        methodNameBox.setAlignment(Pos.CENTER);
        methodNameBox.setPadding(new Insets(10, 10, 10, 10));
        methodNameBox.getChildren().add(methodNameLabel);
        grid.add(methodNameBox, 0, 0, 2, 1);

        grid.add(returnValueLabel, 0, 1);

        grid.add(new Label("Instance to invoke on: "), 0, 2);
        grid.add(objectFor, 1, 2);

        HBox bottomButtons = new HBox(5);
        bottomButtons.setAlignment(Pos.CENTER);
        bottomButtons.setPadding(new Insets(6, 6, 6, 6));
        bottomButtons.getChildren().add(invokeButton);
        grid.add(bottomButtons, 0, 3, 2, 4);
    }
}
