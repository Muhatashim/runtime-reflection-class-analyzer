package org.virus.rtanalyzer.gui;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.virus.rtanalyzer.RTAnalyzer;
import org.virus.rtanalyzer.utils.reflection.JarLoader;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/29/13
 * Time: 10:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class MainUI extends Application {
    public static MainUI             instance;
    private final TreeView<String>   treeView;
    private final TreeItem<String>   classNames;
    private final MetaDataPane       metaDataPane;
    private final InstanceFinderPane instanceFinderPane;
    private final GridPane           grid;
    private       JarLoader          loader;

    public MainUI() {
        grid = new GridPane();
        classNames = new TreeItem<>();
        treeView = new TreeView<>(classNames);
        metaDataPane = new MetaDataPane();
        instanceFinderPane = new InstanceFinderPane();

        classNames.setExpanded(true);

        initUI();

        instance = this;
    }

    public JarLoader getLoader() {
        return loader;
    }

    public InstanceFinderPane getInstanceFinderPane() {
        return instanceFinderPane;
    }

    public void setup(final JarLoader loader) {
        try {
            this.loader = loader;
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            for (Class clazz : loader.getClasses())
                addClass(clazz);

            treeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<String>>() {
                @Override
                public void changed(ObservableValue<? extends TreeItem<String>> observableValue, TreeItem<String> item, TreeItem<String> item2) {
                    TreeItem<String> selectedItem = treeView.getSelectionModel().getSelectedItem();
                    if (selectedItem == null || !selectedItem.getChildren().isEmpty())
                        return;

                    try {
                        Class<?> forClass = Class.forName(toFullyQualifiedName(selectedItem),
                                false, loader.getUrlClassLoader());

                        metaDataPane.setup(forClass);
                        instanceFinderPane.setForClass(forClass);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        RTAnalyzer.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    loader.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void addClass(Class clazz) {
        String name = clazz.getName();
        String[] packages = name.split("\\.");
        ObservableList<TreeItem<String>> childrenList = classNames.getChildren();
        int i;

        for (int pI = 0; pI < packages.length - 1; pI++) {
            String pakage = packages[pI];
            if ((i = findPackageIndex(pakage, childrenList)) != -1)
                childrenList = childrenList.get(i).getChildren();
            else {
                TreeItem<String> stringTreeItem = new TreeItem<>(pakage);
                childrenList.add(stringTreeItem);
                childrenList = stringTreeItem.getChildren();
            }
        }

        childrenList.add(new TreeItem<>(clazz.getSimpleName()));
    }

    private int findPackageIndex(String name, ObservableList<TreeItem<String>> childrenList) {
        for (int i = 0; i < childrenList.size(); i++) {
            TreeItem<String> item = childrenList.get(i);
            if (item.getValue().equals(name))
                return i;
        }
        return -1;
    }

    private String toFullyQualifiedName(TreeItem<String> item) {
        if (item.getValue() == null)
            return "";

        StringBuilder sb = new StringBuilder(item.getValue());
        TreeItem<String> parent = item;
        while ((parent = parent.getParent()) != null && parent.getValue() != null)
            sb.insert(0, parent.getValue() + ".");
        return sb.toString();
    }

    private void initUI() {
        grid.add(treeView, 0, 0);

        HBox infoBox = new HBox();
        infoBox.setAlignment(Pos.CENTER);
        infoBox.setPadding(new Insets(10, 10, 10, 10));
        infoBox.getChildren().addAll(metaDataPane, instanceFinderPane);
        grid.add(infoBox, 1, 0);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(grid));
        stage.show();
    }

}
