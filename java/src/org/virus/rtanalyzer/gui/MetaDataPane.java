package org.virus.rtanalyzer.gui;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.virus.rtanalyzer.utils.reflection.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/29/13
 * Time: 11:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class MetaDataPane extends GridPane {
    private Label             classNameLabel;
    private TableView<Field>  fields;
    private TableView<Method> methods;
    private Button            editFieldButton;
    private Button            getFieldButton;
    private Button            invokeMethodButton;
    private Button            setInstanceForButton;

    private volatile Object instanceFor;

    public MetaDataPane() {
        classNameLabel = new Label();
        fields = new TableView<>();
        methods = new TableView<>();
        editFieldButton = new Button("Edit field value");
        getFieldButton = new Button("Get field value");
        invokeMethodButton = new Button("Invoke method");
        setInstanceForButton = new Button("Set instance");

        editFieldButton.setVisible(false);
        getFieldButton.setVisible(false);
        invokeMethodButton.setVisible(false);

        fields.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        fields.setMinWidth(600);
        methods.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        methods.setMinWidth(600);

        createMemberCols(fields);
        createMemberCols(methods);
        createFieldCols(fields);
        createMethodCols(methods);

        prepareUI();
        addListeners();
    }

    private static void createMethodCols(TableView<Method> methods) {
        TableColumn<Method, String> methodReturnTypeCol = new TableColumn<>("Return type");
        methodReturnTypeCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Method, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Method, String> methodStringCellDataFeatures) {
                return new SimpleStringProperty(
                        methodStringCellDataFeatures.getValue().getReturnType().getName());
            }
        });
        methods.getColumns().add(methodReturnTypeCol);
    }

    private static <T extends Member> void createMemberCols(TableView<T> memberTable) {
        TableColumn<T, String> nameCol = new TableColumn<>("Name");
        TableColumn<T, String> modifiersCol = new TableColumn<>("Modifiers");

        nameCol.setCellValueFactory(
                new PropertyValueFactory<T, String>("name"));

        modifiersCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<T, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<T, String> memberData) {
                int modifiers = memberData.getValue().getModifiers();
                return new SimpleStringProperty(Modifier.toString(modifiers));
            }
        });

        memberTable.getColumns().addAll(modifiersCol, nameCol);
    }

    private void prepareUI() {
        VBox classNameBox = new VBox(5);
        classNameBox.setPadding(new Insets(10, 10, 10, 10));
        classNameBox.getChildren().addAll(classNameLabel);
        classNameBox.setAlignment(Pos.CENTER);
        add(classNameBox, 0, 0);

        VBox fieldsVBox = new VBox(5);
        fieldsVBox.getChildren().add(new Label("Fields:"));
        fieldsVBox.getChildren().add(fields);
        add(fieldsVBox, 0, 1);

        VBox fieldsPropertiesBox = new VBox(5);
        fieldsPropertiesBox.setAlignment(Pos.CENTER);
        fieldsPropertiesBox.setPadding(new Insets(10, 10, 10, 10));
        fieldsPropertiesBox.getChildren().addAll(setInstanceForButton, editFieldButton, getFieldButton);
        add(fieldsPropertiesBox, 1, 1);

        VBox methodsVBox = new VBox(5);
        methodsVBox.getChildren().add(new Label("Methods:"));
        methodsVBox.getChildren().add(methods);
        add(methodsVBox, 0, 2);

        VBox methodsPropertiesBox = new VBox(5);
        methodsPropertiesBox.setAlignment(Pos.CENTER);
        methodsPropertiesBox.setPadding(new Insets(10, 10, 10, 10));
        methodsPropertiesBox.getChildren().add(invokeMethodButton);
        add(methodsPropertiesBox, 1, 2);
    }

    public void setup(Class clazz) {
        instanceFor = null;

        classNameLabel.setText(ReflectionUtils.toString(clazz));

        fields.setItems(FXCollections.observableArrayList(
                clazz.getDeclaredFields()));

        methods.setItems(FXCollections.observableArrayList(
                clazz.getDeclaredMethods()));
    }

    private void addListeners() {
        fields.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Field>() {
            @Override
            public void changed(ObservableValue<? extends Field> observableValue, Field field, Field field2) {
                Field selectedItem = fields.getSelectionModel().getSelectedItem();
                if (selectedItem != null && !Modifier.isFinal(selectedItem.getModifiers())) {
                    Class<?> type = selectedItem.getType();
                    if (type == String.class
                            || type == boolean.class
                            || type == byte.class
                            || type == short.class
                            || type == int.class
                            || type == float.class
                            || type == long.class
                            || type == double.class)
                        editFieldButton.setVisible(true);
                    getFieldButton.setVisible(!Modifier.isStatic(selectedItem.getModifiers()));
                    return;
                }
                editFieldButton.setVisible(false);
            }
        });

        methods.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Method>() {
            @Override
            public void changed(ObservableValue<? extends Method> observableValue, Method method, Method method2) {
                Method selectedItem = methods.getSelectionModel().getSelectedItem();
                invokeMethodButton.setVisible(selectedItem != null
                        && selectedItem.getParameterTypes().length == 0);
            }
        });

        getFieldButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                new GetField(fields.getSelectionModel().getSelectedItem());
            }
        });
        editFieldButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                new ModifyField(fields.getSelectionModel().getSelectedItem());
            }
        });
        invokeMethodButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                new InvokeMethod(methods.getSelectionModel().getSelectedItem());
            }
        });

        setInstanceForButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                instanceFor = MainUI.instance.getInstanceFinderPane().getInstanceSelected();
            }
        });
    }

    private void createFieldCols(TableView<Field> fields) {
        TableColumn<Field, String> fieldTypeCol = new TableColumn<>("Type");
        fieldTypeCol.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Field, String>, ObservableValue<String>>() {
                    final SimpleStringProperty property = new SimpleStringProperty("");

                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<Field, String> fieldStringCellDataFeatures) {
                        property.setValue(null); //javafx caching bug

                        property.set(fieldStringCellDataFeatures.getValue().getType().getName());
                        return property;
                    }
                });


        TableColumn<Field, String> fieldValueCol = new TableColumn<>("Value");
        fieldValueCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Field, String>, ObservableValue<String>>() {
            final SimpleStringProperty property = new SimpleStringProperty("");

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Field, String> fieldStringCellDataFeatures) {
                property.setValue(null); //javafx caching bug

                try {
                    Object value = ReflectionUtils.getValue(fieldStringCellDataFeatures.getValue(), instanceFor);
                    property.setValue(ReflectionUtils.toString(value));
                } catch (Exception e1) {
                    property.setValue(
                            e1 instanceof NullPointerException ?
                                    "Non-static field" : "Unknown");
                }
                return property;
            }
        });

        fields.getColumns().addAll(fieldTypeCol, fieldValueCol);
    }
}
