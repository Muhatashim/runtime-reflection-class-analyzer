package org.virus.rtanalyzer.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.virus.rtanalyzer.utils.reflection.ReflectionUtils;

import javax.swing.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/29/13
 * Time: 3:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class ModifyField {
    private final Stage    stage;
    private final Scene    scene;
    private final GridPane grid;

    private final Label     fieldNameLabel;
    private final TextField objectFor;
    private final Field     field;
    private       TextField fieldValue;
    private final Button    setButton;

    public ModifyField(Field field) {
        this.field = field;
        stage = new Stage();
        grid = new GridPane();

        setButton = new Button("Set value");
        fieldNameLabel = new Label(field.toString());
        try {
            Object value = ReflectionUtils.getValue(field, null);
            fieldValue = new TextField(ReflectionUtils.toString(value));
        } catch (NullPointerException e) {
            fieldValue = new TextField();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            fieldValue = new TextField();
        }
        objectFor = new TextField();
        if (Modifier.isStatic(field.getModifiers())) {
            objectFor.setEditable(false);
            objectFor.setText("null");
        }

        initUI();
        addListeners();

        stage.setResizable(false);
        scene = new Scene(grid);
        stage.setScene(scene);
        stage.show();
    }

    private void addListeners() {
        setButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Class<?> type = field.getType();
                String valueText = fieldValue.getText();
                String forText = objectFor.getText();
                Object instanceFor;

                if (!Modifier.isStatic(field.getModifiers())) {
                    try {
                        instanceFor = ReflectionUtils.getValue(forText);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Couldn't find class!");
                        return;
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(null, "No such field!");
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                } else
                    instanceFor = null;

                try {
                    if (type == String.class)
                        field.set(instanceFor, valueText);
                    if (type == byte.class)
                        field.setByte(instanceFor, Byte.parseByte(valueText));
                    if (type == short.class)
                        field.setShort(instanceFor, Short.parseShort(valueText));
                    if (type == int.class)
                        field.setInt(instanceFor, Integer.parseInt(valueText));
                    if (type == long.class)
                        field.setLong(instanceFor, Long.parseLong(valueText));
                    if (type == float.class)
                        field.setFloat(instanceFor, Float.parseFloat(valueText));
                    if (type == double.class)
                        field.setDouble(instanceFor, Double.parseDouble(valueText));
                } catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Error while trying to set data.");
                    return;
                }

                stage.close();
            }
        });
    }

    private void initUI() {
        HBox nameBox = new HBox();
        nameBox.setPadding(new Insets(10, 10, 10, 10));
        nameBox.getChildren().add(fieldNameLabel);
        grid.add(nameBox, 0, 0, 2, 1);

        grid.add(new Label("Field value: "), 0, 1);
        grid.add(new Label("Object for: "), 0, 2);

        grid.add(fieldValue, 1, 1);
        grid.add(objectFor, 1, 2);

        HBox bottomButtons = new HBox(5);
        bottomButtons.setPadding(new Insets(10, 10, 10, 10));
        bottomButtons.setAlignment(Pos.CENTER);
        bottomButtons.getChildren().add(setButton);
        grid.add(bottomButtons, 0, 3, 2, 4);
    }
}
