package org.virus.rtanalyzer.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/30/13
 * Time: 7:34 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Filter<E> {
    protected abstract boolean accept(E element);

    /**
     * @return true indicating that it is time to exit the loop or false to continue looping.
     */
    protected abstract boolean run(E t);

    public final <T extends Iterable<? extends E>> boolean loop(T arr) {
        for (E element : arr)
            if (accept(element) && run(element))
                return true;
        return false;
    }

    //Not varags cause of heap pollution
    public final <T extends E> boolean loop(T[] elements) {
        for (E element : elements)
            if (accept(element) && run(element))
                return true;
        return false;
    }


    public final <T extends Iterable<? extends E>> List<E> allAccepted(T arr) {
        List<E> accepted = new ArrayList<>();

        for (E element : arr)
            if (accept(element))
                accepted.add(element);
        return accepted;
    }

    public final <T extends Iterable<? extends E>> E getFirstAccepted(T arr) {
        for (E element : arr)
            if (accept(element))
                return element;
        return null;
    }

    public final <T extends E> E getAccepted(T[] arr) {
        for (E element : arr)
            if (accept(element))
                return element;
        return null;
    }

    public final <T extends E> List<E> allAccepted(T[] arr) {
        List<E> accepted = new ArrayList<>();

        for (E element : arr)
            if (accept(element))
                accepted.add(element);
        return accepted;
    }
}
