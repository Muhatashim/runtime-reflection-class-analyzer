package org.virus.rtanalyzer.utils.reflection;

import org.virus.rtanalyzer.utils.Filter;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/29/13
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class JarLoader {

    private final URLClassLoader urlClassLoader;
    private final JarFile        jarFile;
    private       List<Class>    classes;

    public JarLoader(URL jarURL) throws URISyntaxException, IOException {
        jarFile = new JarFile(new File(jarURL.toURI()));
        urlClassLoader = new URLClassLoader(new URL[]{jarURL});
    }

    public List<Class> getClasses() {
        if (classes != null)
            return classes;

        Enumeration<JarEntry> entries = jarFile.entries();
        List<Class> classes = new ArrayList<>();

        while (entries.hasMoreElements()) {
            JarEntry next = entries.nextElement();
            String name = next.getName();
            if (name.endsWith(".class")) {
                try {
                    classes.add(urlClassLoader.loadClass(name.replace(".class", "")));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        return this.classes = classes;
    }

    /**
     * Invokes the main method of the first identified main class.
     * Should be called from on a different thread.
     *
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public void run() throws IllegalAccessException, InstantiationException {
        List<Class> classes = getClasses();

        Filter<Method> methodFilter = new Filter<Method>() {
            @Override
            protected boolean accept(Method method) {
                String method_toString = method.toString();
                return method_toString.endsWith("main(java.lang.String[])")
                        && (method_toString).startsWith("public static") && method_toString.contains("void");
            }

            @Override
            protected boolean run(Method method) {
                try {
                    method.invoke(null, new String[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        };
        for (final Class clazz : classes) {
            Method[] methods = clazz.getMethods();
            if (methodFilter.loop(methods))
                return;
        }
    }

    public URLClassLoader getUrlClassLoader() {
        return urlClassLoader;
    }
}
