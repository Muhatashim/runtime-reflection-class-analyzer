package org.virus.rtanalyzer.utils.reflection;

import org.virus.rtanalyzer.gui.MainUI;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/29/13
 * Time: 1:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class ReflectionUtils {

    public static String toString(Class clazz) {
        Class superclass = clazz.getSuperclass();
        List<String> interfaces = new ArrayList<>();

        for (Class iface : clazz.getInterfaces())
            interfaces.add(iface.getName());

        return clazz.toString() + (superclass != null ? " extends " + superclass.getName() : "") + "\n\t\t"
                + "implements " + interfaces.toString();
    }

    public static Object invoke(Method method, Object instance) throws IllegalAccessException, InvocationTargetException {
        method.setAccessible(true);
        return method.invoke(instance);
    }

    public static String toString(Object obj) {
        String strVal;

        if (obj.getClass().isArray())
            strVal = Arrays.deepToString((Object[]) obj);
        else
            strVal = obj.toString();
        return strVal;
    }

    public static Object getValue(Field field, Object instance) throws IllegalAccessException {
        if (!field.isAccessible())
            field.setAccessible(true);
        return field.get(instance);
    }

    public static Object getValue(String fullFieldName) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        int lastIdxDot = fullFieldName.lastIndexOf('.');
        String className = fullFieldName;
        String fieldName = null;

        if (lastIdxDot == -1)
            return null;

        if (!className.isEmpty() && !className.equals("null")) {
            className = fullFieldName.substring(0, lastIdxDot);
            fieldName = fullFieldName.substring(lastIdxDot + 1);
        }

        Class<?> aClass = null;
        if (fieldName != null)
            aClass = Class.forName(className, false, MainUI.instance.getLoader().getUrlClassLoader());

        if (aClass != null) {
            Field declaredField = aClass.getDeclaredField(fieldName);
            declaredField.setAccessible(true);
            return declaredField.get(aClass);
        }
        return null;
    }
}
